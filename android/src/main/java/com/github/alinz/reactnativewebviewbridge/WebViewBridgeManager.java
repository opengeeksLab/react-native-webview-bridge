package com.github.alinz.reactnativewebviewbridge;

import android.content.Context;
import android.os.Build;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
//import android.webkit.WebViewClient;

import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.views.webview.ReactWebViewManager;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Nullable;

import static android.view.View.FOCUS_DOWN;
import static android.view.View.FOCUS_UP;

public class WebViewBridgeManager extends ReactWebViewManager {
    private static final String REACT_CLASS = "RCTWebViewBridge";
    private Context context;
    private WebView rootWebView;
    private boolean isFocused = false;

    public static final int COMMAND_SEND_TO_BRIDGE = 101;

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public
    @Nullable
    Map<String, Integer> getCommandsMap() {
        Map<String, Integer> commandsMap = super.getCommandsMap();

        commandsMap.put("sendToBridge", COMMAND_SEND_TO_BRIDGE);

        return commandsMap;
    }

    @ReactProp(name="focus")
    public void setFocus(WebView root, boolean isFocus) {
        if (isFocus && !this.isFocused) {
            root.requestFocus(FOCUS_UP);
            InputMethodManager inputMethodManager = (InputMethodManager) this.context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }

        if (!isFocus && this.isFocused) {
            root.requestFocus(FOCUS_DOWN);
        }

        this.isFocused = isFocus;
    }


    @Override
    protected WebView createViewInstance(ThemedReactContext reactContext) {
        this.context = reactContext;

        WebView root = super.createViewInstance(reactContext);
        root.addJavascriptInterface(new JavascriptBridge(root), "WebViewBridge");
        if (Build.VERSION.SDK_INT > 21) {
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptThirdPartyCookies(root, true);
        }

//        root.setWebViewClient(new WebViewClient());
        root.setWebChromeClient(new WebChromeClient());

        root.getSettings().setGeolocationEnabled(true);
        root.getSettings().setAppCacheEnabled(true);
        root.getSettings().setDatabaseEnabled(true);
        root.getSettings().setDomStorageEnabled(true);
        root.getSettings().setAllowUniversalAccessFromFileURLs(true);

        root.getSettings().setJavaScriptEnabled(true);
        root.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        this.rootWebView = root;
        return root;
    }

    @Override
    public void receiveCommand(WebView root, int commandId, @Nullable ReadableArray args) {
        super.receiveCommand(root, commandId, args);

        switch (commandId) {
            case COMMAND_SEND_TO_BRIDGE:
                sendToBridge(root, args.getString(0));
                break;
            default:
                //do nothing!!!!
        }
    }

    private void sendToBridge(WebView root, String message) {
        String script = "WebViewBridge.onMessage('" + message + "');";
        WebViewBridgeManager.evaluateJavascript(root, script);
    }

    static private void evaluateJavascript(WebView root, String javascript) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            root.evaluateJavascript(javascript, null);
        } else {
            root.loadUrl("javascript:" + javascript);
        }
    }

    @ReactProp(name = "allowFileAccessFromFileURLs")
    public void setAllowFileAccessFromFileURLs(WebView root, boolean allows) {
        root.getSettings().setAllowFileAccessFromFileURLs(allows);
    }

    @ReactProp(name = "allowUniversalAccessFromFileURLs")
    public void setAllowUniversalAccessFromFileURLs(WebView root, boolean allows) {
        root.getSettings().setAllowUniversalAccessFromFileURLs(allows);
    }
}